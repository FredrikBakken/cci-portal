'''
Crack the Coding Interview 6th Edition

Data Structures | Chapter 1 | Arrays and Strings

Challenge 9: String Rotation
Assume you have a method isSubstring which checks if one word is
a substring of another. Given two strings, s1 and s2, write code
to check if s2 is a rotation of s1 using only one call to
isSubstring (e.g. "waterbottle" is a rotation of "erbottlewat").
'''

from copy import deepcopy

from challenge_9.solution import solution


def execute_challenge_9(module, generate_random_test, print_results, custom_test_input, number_of_random):
    if isinstance(custom_test_input, str):
        custom_test = False
        
        # Generate known tests
        tests = [
            ['waterbottle', 'erbottlewat'],
            ['waterbottle', 'rebottlewat'],
            ['God is good', ' is goodGod'],
            ['Imagination is more important than knowledge.', ' more important than knowledge. isImagination'],
            ['Look deep into nature, and then you will understand everything better', 'into nature, and then you will understand everything betterLook deep ']
        ]
    else:
        custom_test = True

        # Generate custom test
        tests = [custom_test_input]
    
    # Custom test, if less than 5 random tests
    if number_of_random < 5:
        custom_test = True

    # Generate and append random testcases
    for _ in range(number_of_random):
        tests.append(generate_random_test(minStringLength=10, maxStringLength=30, string_rotation=True))

    # Deepcopy tests to create new memory allocations
    test_solution = deepcopy(tests)
    test_suggestion = deepcopy(tests)

    # Initialize results
    results = []

    # Loop through each test
    for index, test in enumerate(tests):
        # Fetch solution linked list
        answer = solution(test_solution[index][0], test_solution[index][1])

        # Fetch suggestion linked list
        suggestion = module.string_rotation(test_suggestion[index][0], test_suggestion[index][1])

        # Input / Output as string variable
        test_input = test
        test_answer = answer
        test_output = suggestion

        # Append current test results to results
        results.append([test_input, test_answer, test_output])
    
    # Print the results
    print_results(results, custom_test, number_of_random, two_inputs=True)
