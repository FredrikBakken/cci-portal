'''
Crack the Coding Interview 6th Edition

Data Structures | Chapter 1 | Arrays and Strings

Challenge 4: Palindrome Permutation
Given a string, write a function to check if it is a permutation of a
palindrome. A palindrome is a word or phrase that is the same forwards
and backwards. A permutation is a rearrangement of letters. The
palindrome does not need to be limited to just dictionary words.

EXAMPLE
Input: Tact Coa
Output: True (permutations: "taco cat", "atco cta", etc.)
'''

from copy import deepcopy

from challenge_4.solution import solution


def execute_challenge_4(module, generate_random_test, print_results, custom_test_input, number_of_random):
    if isinstance(custom_test_input, str):
        custom_test = False
        
        # Generate known tests
        tests = [
            ["yelloweowlly"],
            ["redividers"],
            ["madorvvedmaro"],
            ["step on penots "],
            ["racecar"]
        ]
    else:
        custom_test = True

        # Generate custom test
        tests = [custom_test_input]

    # Generate and append random testcases
    for _ in range(number_of_random):
        tests.append(generate_random_test(minStringLength=10, maxStringLength=20, permutation=True, palindrome=True))

    # Deepcopy tests to create new memory allocations
    test_solution = deepcopy(tests)
    test_suggestion = deepcopy(tests)

    # Initialize results
    results = []

    # Loop through each test
    for index, test in enumerate(tests):
        # Fetch solution linked list
        answer = solution(test_solution[index][0])

        # Fetch suggestion linked list
        suggestion = module.palindrome_permutation(test_suggestion[index][0])

        # Input / Output as string variable
        test_input = test[0]
        test_answer = answer
        test_output = suggestion

        # Append current test results to results
        results.append([test_input, test_answer, test_output])
    
    # Print the results
    print_results(results, custom_test, number_of_random, one_input=True)
