'''
Crack the Coding Interview 6th Edition

Data Structures | Chapter 1 | Arrays and Strings

Challenge 2: Check Permutation
Given two strings, write a method to decide if one is a permutation of
the other.
'''

from copy import deepcopy

from challenge_2.solution import solution


def execute_challenge_2(module, generate_random_test, print_results, custom_test_input, number_of_random):
    if isinstance(custom_test_input, str):
        custom_test = False
        
        # Generate known tests
        tests = [
            ["Hello", "oelHl"],
            ["My name is Alice", "yAme ina sM cile"],
            ["I am from Norway", "IormN f'ma yawo"],
            ["Your favorite food is pizza with ananas", "ananas is Your food favorite with pizza"],
            ["Please, don't throw that in the Ocean", "throw Ocean in Plastic don't the"]
        ]
    else:
        custom_test = True

        # Generate custom test
        tests = [custom_test_input]

    # Generate and append random testcases
    for _ in range(number_of_random):
        tests.append(generate_random_test(minStringLength=5, maxStringLength=25, permutation=True))

    # Deepcopy tests to create new memory allocations
    test_solution = deepcopy(tests)
    test_suggestion = deepcopy(tests)

    # Initialize results
    results = []

    # Loop through each test
    for index, test in enumerate(tests):
        # Fetch solution linked list
        answer = solution(test_solution[index][0], test_solution[index][1])

        # Fetch suggestion linked list
        suggestion = module.check_permutation(test_suggestion[index][0], test_solution[index][1])

        # Input / Output as string variable
        test_input = test
        test_answer = answer
        test_output = suggestion

        # Append current test results to results
        results.append([test_input, test_answer, test_output])
    
    # Print the results
    print_results(results, custom_test, number_of_random, two_inputs=True)
