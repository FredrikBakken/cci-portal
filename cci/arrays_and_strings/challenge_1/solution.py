def solution(input_string):
    # Loop through the entire input string
    for i in range(0, len(input_string)):
        # Define the current character
        current_character = input_string[i]

        # Loop through the remaining input string (after current)
        for j in range(i + 1, len(input_string)):
            # Define the current check character
            check_character = input_string[j]

            # Check if current and check character are the same
            if check_character == current_character:
                # Return result as False (not unique)
                return False
    
    # Return result as True (unique)
    return True
