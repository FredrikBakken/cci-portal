# Imports
import argparse

from importlib import import_module

from support import generate_random_test, print_results, custom_test_setup, number_of_random_runs

from challenge_1.is_unique import execute_challenge_1
from challenge_2.check_permutation import execute_challenge_2
from challenge_3.urlify import execute_challenge_3
from challenge_4.palindrome_permutation import execute_challenge_4
from challenge_5.one_away import execute_challenge_5
from challenge_6.string_compression import execute_challenge_6
from challenge_7.rotate_matrix import execute_challenge_7
from challenge_8.zero_matrix import execute_challenge_8
from challenge_9.string_rotation import execute_challenge_9


if __name__ == '__main__':
    # Initialize argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-challenge', metavar='-c')
    parser.add_argument('-file', metavar='-f')
    parser.add_argument('-test', metavar='-t')
    parser.add_argument('-number', metavar='-n')
    args = parser.parse_args()

    # Fetch challenge as integer
    challenge = int(args.challenge)

    # Fetch file to load as module
    filename = str(args.file)

    # Fetch and format user's test input
    custom_test_input = str(args.test)
    custom_test_input = custom_test_setup(custom_test_input, challenge)

    # Fetch the number of random tests
    number_of_random = str(args.number)
    number_of_random = number_of_random_runs(args.number, custom_test_input)
    
    # Load the correct module
    module_name = 'challenge_' + str(challenge) + '.suggestions.' + filename.replace('.py', '')
    module = import_module(module_name)

    # Execute challenge 1
    if challenge == 1:
        execute_challenge_1(module, generate_random_test, print_results, custom_test_input, number_of_random)
    
    # Execute challenge 2
    elif challenge == 2:
        execute_challenge_2(module, generate_random_test, print_results, custom_test_input, number_of_random)
    
    # Execute challenge 3
    elif challenge == 3:
        execute_challenge_3(module, generate_random_test, print_results, custom_test_input, number_of_random)
    
    # Execute challenge 4
    elif challenge == 4:
        execute_challenge_4(module, generate_random_test, print_results, custom_test_input, number_of_random)
    
    # Execute challenge 5
    elif challenge == 5:
        execute_challenge_5(module, generate_random_test, print_results, custom_test_input, number_of_random)

    # Execute challenge 6
    elif challenge == 6:
        execute_challenge_6(module, generate_random_test, print_results, custom_test_input, number_of_random)

    # Execute challenge 7
    elif challenge == 7:
        execute_challenge_7(module, generate_random_test, print_results, custom_test_input, number_of_random)

    # Execute challenge 8
    elif challenge == 8:
        execute_challenge_8(module, generate_random_test, print_results, custom_test_input, number_of_random)

    # Execute challenge 9
    elif challenge == 9:
        execute_challenge_9(module, generate_random_test, print_results, custom_test_input, number_of_random)
