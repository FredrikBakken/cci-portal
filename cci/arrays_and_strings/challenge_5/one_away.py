'''
Crack the Coding Interview 6th Edition

Data Structures | Chapter 1 | Arrays and Strings

Challenge 5: One Away
There are three types of edits that can be performed on strings:
insert a character, remove a character, or replace a character. Given
two strings, write a function to check if they are one edit (or zero
edits) away.
'''

from copy import deepcopy

from challenge_5.solution import solution


def execute_challenge_5(module, generate_random_test, print_results, custom_test_input, number_of_random):
    if isinstance(custom_test_input, str):
        custom_test = False
        
        # Generate known tests
        tests = [
            ["Hello World", "Hello World!"],
            ["abcdefghijklmnopq", "abcdefghijklmnopqr"],
            ["Bacon ipsum dolor amet flank strip steak", "Bacon ipsum dolor flank strip steak"],
            ["Look at the stars and not down at your feet", "Look at the star and not down at your feet"],
            ["We don't need no education", "We do not need no education"]
        ]
    else:
        custom_test = True

        # Generate custom test
        tests = [custom_test_input]

    # Generate and append random testcases
    for _ in range(number_of_random):
        tests.append(generate_random_test(minStringLength=10, maxStringLength=20, one_away=True))

    # Deepcopy tests to create new memory allocations
    test_solution = deepcopy(tests)
    test_suggestion = deepcopy(tests)

    # Initialize results
    results = []

    # Loop through each test
    for index, test in enumerate(tests):
        # Fetch solution linked list
        answer = solution(test_solution[index][0], test_solution[index][1])

        # Fetch suggestion linked list
        suggestion = module.one_away(test_suggestion[index][0], test_suggestion[index][1])

        # Input / Output as string variable
        test_input = test
        test_answer = answer
        test_output = suggestion

        # Append current test results to results
        results.append([test_input, test_answer, test_output])
    
    # Print the results
    print_results(results, custom_test, number_of_random, two_inputs=True)
