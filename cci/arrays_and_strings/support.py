# Imports
import ast

from random import randint, choice, sample


# Method to generate random tests
def generate_random_test(minStringLength, maxStringLength, permutation=False, palindrome=False, one_away=False, string_compression=False, matrix=False, m_n=False, string_rotation=False):
    def random_string(stringLength):
        # Initialize string
        random_string = ''

        # Loop in order to generate each character in the test string
        for _ in range(stringLength):
            # Generate a random character from ASCII-values
            random_string += chr(randint(32, 126))
        
        # Return randomly generated string
        return random_string


    # Set the length of the generated string
    stringLength = randint(minStringLength, maxStringLength)

    # Initialize the test string
    if not string_compression and not matrix:
        test_string = random_string(stringLength)
    
    
    # If permutation is defined
    if permutation:
        # Initialize the permutation string
        string_permutation = ''

        # Define if current string should be permutation or not
        is_permutation = choice([True, False])

        # Create permutation string
        if is_permutation:
            string_permutation = ''.join(sample(test_string, stringLength))
        # Create a new random string
        else:
            string_permutation = random_string(stringLength)

        # If palindrome is defined
        if palindrome:
            # Initialize palindrome permutation string
            palindrome_permutation = test_string
            
            # Chance to add a middle character
            if choice([True, False]):
                palindrome_permutation += random_string(1)
            
            # Append ending of palindrome permutation string
            palindrome_permutation += string_permutation

            # Return palindrome permutation test
            return [palindrome_permutation]
        
        # Return permutation test
        return [test_string, string_permutation]
    
    # If one away is defined
    if one_away:
        # Initialize the one away string
        one_away_string = ''

        # Loop in order to generate each character in the test string
        for i in range(0, len(test_string)):
            # Add character to string
            if randint(0, 44) == 44:
                one_away_string += chr(randint(32, 126))

            # Remove character from string
            if randint(0, 44) != 44:
                one_away_string += test_string[i]
        
        # Return one away test
        return [test_string, one_away_string]

    # If string compression is defined
    if string_compression:
        # Initialize the string compression string
        compression_string = ''

        # Loop in order to generate each character in the test string
        for _ in range(stringLength):
            # Generate a random character from ASCII-values
            random_character = chr(randint(65, 122))

            # Number of characters in a row
            number_of_characters = randint(0, 4)

            # Randomly append the same random characters x times
            if number_of_characters <= 2:
                compression_string += random_character
            else:
                for _ in range(number_of_characters):
                    compression_string += random_character
        
        # Return the generated string compression test
        return [compression_string]
    
    # If matrix is defined
    if matrix:
        # Matrix placeholder
        matrix_test = []

        # Iterate the rows of the matrix
        for _ in range(stringLength):
            # Define current row list
            current_row = []

            # Initialize max value
            max_value = 255

            # If mxn matrix
            if m_n:
                # Update max value
                max_value = 15

                # Update for number of columns
                if choice([True, False]):
                    stringLength = randint(minStringLength*2, maxStringLength*2)
                else:
                    stringLength = randint(minStringLength//2, maxStringLength//2)
            
            # Iterate the columns of the matrix
            for _ in range(stringLength):
                current_row.append(randint(0, max_value))
            
            # Append row to matrix
            matrix_test.append(current_row)
        
        # Return the generated matrix test
        return matrix_test

    # If string rotation is defined
    if string_rotation:
        # Initialize the string rotation variable
        rotation_string = ''

        # 50% chance for rotation or no rotation
        if choice([True, False]):
            split_index = randint(0, len(test_string))
            rotation_string = test_string[split_index:] + test_string[:split_index]
        else:
            rotation_string = random_string(stringLength)
        
        # Return the generated string rotation test
        return [test_string, rotation_string]
    
    # Return the generated random test
    return [test_string]


# Print results to result output
def print_results(output, custom_test, number_of_random, one_input=False, two_inputs=False, matrix=False):
    def print_output(output):
        # Initialize solved variable
        solved = True

        # Loop print input/output results
        for index, answer in enumerate(output):
            print('Testcase ' + str(index + 1) + ':')
            # One input
            if one_input:
                print('Input: ' + str(answer[0]))
                print('Output (solution): ' + str(answer[1]))
                print('Output (suggestion): ' + str(answer[2]))
            # Two inputs
            elif two_inputs:
                print('Input 1: ' + str(answer[0][0]))
                print('Input 2: ' + str(answer[0][1]))
                print('Output (solution): ' + str(answer[1]))
                print('Output (suggestion): ' + str(answer[2]))
            # Matrix input
            elif matrix:
                print('Input matrix:')
                for row in answer[0]:
                    print(row)
                print('Output matrix (solution):')
                for row in answer[1]:
                    print(row)
                print('Output matrix (suggestion):')
                for row in answer[2]:
                    print(row)
            print('Correct answer? ' + str(answer[1]==answer[2]) + '\n')

            # Update solved if incorred
            if answer[1] != answer[2]:
                solved = False
        
        # Return solved status
        return solved


    # Initialize printing of results
    print('---RESULTS---')

    # Print to result textarea and fetch solved status
    solved = print_output(output)
    
    # Custom test, if less than 5 random tests
    if number_of_random < 5:
        custom_test = True

    # Print correct/incorrect
    if solved and not custom_test:
        print('CORRECT')
    elif solved and custom_test:
        print('TEST CORRECT')
    elif not solved and custom_test:
        print('TEST INCORRECT')
    else:
        print('INCORRECT')


# Setup the test data based on challenge
def custom_test_setup(custom_test_input, challenge):
    # If a custom test input is posted
    if not custom_test_input == '':
        # If single input challenge
        if challenge in [1, 3, 4, 6]:
            return [custom_test_input]
        
        # If two inputs challenge
        elif challenge in [2, 5, 9]:
            return custom_test_input.replace('; ', ';').split(';')
        
        # If matrix input challenge
        elif challenge in [7, 8]:
            return ast.literal_eval(custom_test_input)
    
    # Return the unformatted input
    return custom_test_input


# Get the number of run for generating random tests
def number_of_random_runs(number_of_random, custom_test_input):
    # Try to convert variable to integer
    def tryint(s):
        try:
            return int(s)
        except ValueError:
            return s
    
    # Check if number of random is integer
    number_of_random = tryint(number_of_random)

    # Find and return the number of random tests to generate
    if isinstance(custom_test_input, str) and isinstance(number_of_random, int) or isinstance(custom_test_input, list) and isinstance(number_of_random, int):
        return number_of_random
    elif isinstance(custom_test_input, str) and isinstance(number_of_random, str):
        return 5
    else:
        return 0
