'''
Crack the Coding Interview 6th Edition

Data Structures | Chapter 1 | Arrays and Strings

Challenge 6: String Compression
Implement a method to perform basic string compression using the
counts of repeated characters. For example, the string aabcccccaaa
would become a2b1c5a3. If the 'compressed' string would not become
smaller than the original string, your method should return the
original string. You can assume the string has only uppercase and
lowercase letters (a - z).
'''

from copy import deepcopy

from challenge_6.solution import solution


def execute_challenge_6(module, generate_random_test, print_results, custom_test_input, number_of_random):
    if isinstance(custom_test_input, str):
        custom_test = False
        
        # Generate known tests
        tests = [
            ["aaabbcccc"],
            ["abc"],
            ["xxyyzz"],
            ["abbreeeee"],
            ["mmiissssiissssiippppii"]
        ]
    else:
        custom_test = True

        # Generate custom test
        tests = [custom_test_input]

    # Generate and append random testcases
    for _ in range(number_of_random):
        tests.append(generate_random_test(minStringLength=3, maxStringLength=5, string_compression=True))

    # Deepcopy tests to create new memory allocations
    test_solution = deepcopy(tests)
    test_suggestion = deepcopy(tests)

    # Initialize results
    results = []

    # Loop through each test
    for index, test in enumerate(tests):
        # Fetch solution linked list
        answer = solution(test_solution[index][0])

        # Fetch suggestion linked list
        suggestion = module.string_compression(test_suggestion[index][0])

        # Input / Output as string variable
        test_input = test[0]
        test_answer = answer
        test_output = suggestion

        # Append current test results to results
        results.append([test_input, test_answer, test_output])
    
    # Print the results
    print_results(results, custom_test, number_of_random, one_input=True)
