def solution(input_string_1, input_string_2):

    # Check if the input strings are equal
    if input_string_1 == input_string_2:
        return 0
    
    # Keep track of the number of differences
    change_counter = 0

    # Find longest input and set string_length to shortest
    if len(input_string_1) != len(input_string_2):
        change_counter = abs(len(input_string_1) - len(input_string_2))

    # Check if there are more than 1 length difference
    if change_counter > 1:
        return 2

    # Find the minimum string length
    string_length = min(len(input_string_1), len(input_string_2))

    # If only last character is different
    if input_string_1[:string_length] == input_string_2[:string_length]:
        return 1

    # Initialize number of change operations necessary
    change_operations = 0

    # Loop through the minimum string length
    for i in range(0, string_length):
        # Compare each character in the two strings
        if input_string_1[i] != input_string_2[i]:
            
            # Difference in characters, update change_operations
            change_operations += 1

            # Check if more than 1 operations has been triggered
            if change_operations > 1:
                return 2

            # Insert a character
            insert_test = input_string_1[:i] + input_string_2[i] + input_string_1[i:]
            
            # Remove a character
            remove_test = input_string_1[:i] + input_string_1[i+1:]

            # Replace a character
            replace_test = input_string_1[:i] + input_string_2[i] + input_string_1[i+1:]

            # Only one change operation needed
            if ((insert_test == input_string_2) or (remove_test == input_string_2) or (replace_test == input_string_2)):
                return 1

    # All other cases
    return 2
