def return_kth_to_last(head, k):
    print('Kth element: ' + str(k))

    # Define current node
    current = head
    
    # Loop until the end of the linked list
    while current != None:
        print(current.data)
        current = current.next
    
    # Return the head data of the linked list
    return head
