def solution(input_string):
    # Remove spaces to the right and replace all spaces
    return input_string.rstrip().replace(' ', '%20')
