'''
Crack the Coding Interview 6th Edition

Data Structures | Chapter 1 | Arrays and Strings

Challenge 7: Rotate Matrix
Given an image represented by an NxN matrix, where each pixel in
the image is 4 bytes, write a method to rotate the image by 90
degrees. Can you do this in place?
'''

from copy import deepcopy

from challenge_7.solution import solution


def execute_challenge_7(module, generate_random_test, print_results, custom_test_input, number_of_random):
    if isinstance(custom_test_input, str):
        custom_test = False
        
        # Generate known tests
        tests = [
            [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
            [[0, 0, 0, 0], [1, 1, 1, 1], [2, 2, 2, 2], [3, 3, 3, 3]],
            [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H', 'I']],
            [[0, 50, 100, 150, 200], [50, 100, 150, 200, 0], [100, 150, 200, 0, 50], [150, 200, 0, 50, 100], [200, 0, 50, 100, 150]],
            [['2B', '9C', 'E4'], ['B2', 'K0', '4T'], ['M2', 'L3', 'Q8']]
        ]
    else:
        custom_test = True

        # Generate custom test
        tests = [custom_test_input]

    # Generate and append random testcases
    for _ in range(number_of_random):
        tests.append(generate_random_test(minStringLength=3, maxStringLength=6, matrix=True))

    # Deepcopy tests to create new memory allocations
    test_solution = deepcopy(tests)
    test_suggestion = deepcopy(tests)

    # Initialize results
    results = []

    # Loop through each test
    for index, test in enumerate(tests):
        # Fetch solution linked list
        answer = solution(test_solution[index])

        # Fetch suggestion linked list
        suggestion = module.rotate_matrix(test_suggestion[index])

        # Input / Output as string variable
        test_input = test
        test_answer = answer
        test_output = suggestion

        # Append current test results to results
        results.append([test_input, test_answer, test_output])
    
    # Print the results
    print_results(results, custom_test, number_of_random, matrix=True)
