'''
Crack the Coding Interview 6th Edition

Data Structures | Chapter 1 | Arrays and Strings

Challenge 3: URLify
Write a method to replace all spaces in a string with '%20'. You may
assume that the string has sifficient space at the end to hold the
additional characters, and that you are given the "true" length of the
string.

EXAMPLE
Input: "Mr John Smith     ", 13
Output: "Mr%20John%20Smith"
'''

from copy import deepcopy

from challenge_3.solution import solution


def execute_challenge_3(module, generate_random_test, print_results, custom_test_input, number_of_random):
    if isinstance(custom_test_input, str):
        custom_test = False
        
        # Generate known tests
        tests = [
            ["Mr John Smith"],
            ["I would like to order a pizza"],
            ["Ananas"],
            ["Look at all those spaces    "],
            ["Norway is located in Scandinavia"]
        ]
    else:
        custom_test = True

        # Generate custom test
        tests = [custom_test_input]

    # Generate and append random testcases
    for _ in range(number_of_random):
        tests.append(generate_random_test(minStringLength=10, maxStringLength=35))

    # Deepcopy tests to create new memory allocations
    test_solution = deepcopy(tests)
    test_suggestion = deepcopy(tests)

    # Initialize results
    results = []

    # Loop through each test
    for index, test in enumerate(tests):
        # Fetch solution linked list
        answer = solution(test_solution[index][0])

        # Fetch suggestion linked list
        suggestion = module.urlify(test_suggestion[index][0])

        # Input / Output as string variable
        test_input = test[0]
        test_answer = answer
        test_output = suggestion

        # Append current test results to results
        results.append([test_input, test_answer, test_output])
    
    # Print the results
    print_results(results, custom_test, number_of_random, one_input=True)
