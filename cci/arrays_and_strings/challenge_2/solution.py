def solution(input_string_1, input_string_2):
    # Check length of the two input strings
    if len(input_string_1) != len(input_string_2):
        # Return result as False (not permutation)
        return False
    
    # Sort the characters in the two input strings
    sorted_input_string_1 = sorted(input_string_1)
    sorted_input_string_2 = sorted(input_string_2)

    # Loop through all input string characters
    for i in range(0, len(sorted_input_string_1)):
        # Check if each character is equal to the corresponding input string
        if sorted_input_string_1[i] != sorted_input_string_2[i]:
            # Return result as False (not permutation)
            return False
    
    # Return result as True (permutation)
    return True
