def solution(head):
    # Define current node
    current = head
    
    # Loop until the end of the linked list
    while current != None:
        # Define runner node
        runner = current.next
        # Define previous node, as node behind runner node
        previous = current

        # Loop until runner reaches the end of the linked list
        while runner != None:
            # Check if data in current is equal to data in runner node
            if current.data == runner.data:
                # If runner.next node exists
                if runner.next:
                    # Update previous.next
                    previous.next = runner.next
                # If runner.next node does not exist
                else:
                    # Update previous.next to None
                    previous.next = None

            # Update runner node
            runner = runner.next
            # Update previous node
            previous = previous.next
    
        # Update current node
        current = current.next
    
    # Return the head of the linked list
    return head
