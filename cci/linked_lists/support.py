# Imports
from random import randint


# Initialize the Node object
class Node(object):
    def __init__(self, data):
        self.data = data
        self.next = None


# Method to generate known tests
def generate_known_test(elements):
    # Initialize linked list
    head = Node(data=elements[0])

    # Set current node equal to the head node
    current = head

    # Loop through the remaining elements
    for i in range(1, len(elements)):
        # Set the current node's next to a new Node object
        current.next = Node(data=elements[i])
        
        # Update current node to next node
        current = current.next

    # Return the test
    return head


# Method to generate random tests
def generate_random_test(minLength, maxLength, kth=False):
    # Generate a new linked list node
    def new_node():
        return Node(data=chr(randint(32, 126)))

    # Randomly generate the linked list length
    list_length = randint(minLength, maxLength)

    # Specify the linked list head
    head = new_node()

    # Set current node to the head
    current = head

    # Loop through the remaining length of the linked list
    for _ in range(list_length - 1):
        # Set the values for the next node
        current.next = new_node()

        # Update current node value to the next node
        current = current.next
    
    # If kth to last is defined
    if kth:
        # Specify the kth to last value to find
        kth_value = randint(-3, maxLength + 5)

        # Return the kth value and head of the linked list
        return [kth_value, head]

    # Return the head of the linked list
    return head


def linked_list_to_string(linked_list):
    # Initialize the linked list items
    contents = ''

    # Loop until the end of linked list
    while linked_list != None:
        contents += str(linked_list.data) + ', '
        linked_list = linked_list.next
    
    # Return linked list as string
    return contents[:-2]


# Print results to result output
def print_results(output, custom_test, number_of_random, ch1=False, ch2=False):
    def print_output(output):
        # Initialize solved variable
        solved = True

        # Loop print input/output results
        for index, answer in enumerate(output):
            print('Testcase ' + str(index + 1) + ':')
            if ch1:
                if (answer[0] and answer[2]==answer[3]) == False:
                    solved = False
                print('Input (linked list): ' + str(answer[1]))
                print('Output (solution): ' + str(answer[2]))
                print('Output (suggestion): ' + str(answer[3]))
                
            if ch2:
                if (answer[1] != answer[2]):
                    solved = False
                print('Input (kth element): ' + str(answer[0][0]))
                print('Input (linked list): ' + str(answer[0][1]))
                print('Output (solution): ' + str(answer[1]))
                print('Output (suggestion): ' + str(answer[2]))
            
            print('Correct answer? ' + str(solved) + '\n')
        
        # Return solved status
        return solved
    

    # Initialize printing of results
    print('---RESULTS---')

    # Print to result textarea and fetch solved status
    solved = print_output(output)
    
    # Custom test, if less than 5 random tests
    if number_of_random < 5:
        custom_test = True

    # Print correct/incorrect
    if solved and not custom_test:
        print('CORRECT')
    elif solved and custom_test:
        print('TEST CORRECT')
    elif not solved and custom_test:
        print('TEST INCORRECT')
    else:
        print('INCORRECT')


def custom_test_setup(custom_test_input, challenge):
    # If a custom test input is posted
    if not custom_test_input == '':
        if challenge in [1]:
            return [generate_known_test(elements=custom_test_input.replace(', ', ',').split(','))]
        elif challenge in [2]:
            custom_test_input = custom_test_input.replace(', ', ',').split(';')
            return [custom_test_input[0], generate_known_test(elements=custom_test_input.split(','))]
    
    # Return the unformatted input
    return custom_test_input


# Get the number of run for generating random tests
def number_of_random_runs(number_of_random, custom_test_input):
    # Try to convert variable to integer
    def tryint(s):
        try:
            return int(s)
        except ValueError:
            return s
    
    # Check if number of random is integer
    number_of_random = tryint(number_of_random)

    # Find and return the number of random tests to generate
    if isinstance(custom_test_input, str) and isinstance(number_of_random, int) or isinstance(custom_test_input, list) and isinstance(number_of_random, int):
        return number_of_random
    elif isinstance(custom_test_input, str) and isinstance(number_of_random, str):
        return 5
    else:
        return 0
