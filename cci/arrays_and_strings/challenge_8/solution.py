def solution(matrix):
    # Initialize lists with rows and cols to edit
    rows_to_edit = []
    cols_to_edit = []

    # Loop through each row in the matrix
    for i in range(0, len(matrix)):
        # Loop through each column in the matrix
        for j in range(0, len(matrix[i])):
            # If current cell is 0
            if matrix[i][j] == 0:
                # Append row number if it does not exist
                if i not in rows_to_edit:
                    rows_to_edit.append(i)
                
                # Append column number if it does not exist
                if j not in cols_to_edit:
                    cols_to_edit.append(j)

    # Loop through each row in the matrix
    for i in range(0, len(matrix)):
        # Loop through each column in the matrix
        for j in range(0, len(matrix[i])):
            # If current cell number is in either of the edit lists
            if ((i in rows_to_edit) or (j in cols_to_edit)):
                # Set that cell value to 0
                matrix[i][j] = 0

    # Return the updated matrix
    return matrix
