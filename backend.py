# Imports
import os
import re
import subprocess

from flask import request


def challenge_status(challenge_location):
    # Dictionary of challenge statuses
    challenge_statuses = {}

    # Loop through all files and directories
    for root, dirs, files in os.walk(challenge_location):
        for filename in files:
            if filename.endswith('.txt'):
                # Get the challenge number
                challenge_number = root.replace(challenge_location + 'challenge_', '').replace('\suggestions', '')

                # Set the initial message
                challenge_statuses[challenge_number] = 'Unanswered'

                # Open the current file
                with open(root + '/' + filename) as f:
                    lines = f.readlines()

                    # Check if file has more than 1 line
                    if len(lines) > 1:
                        # Update the challenge status to Incorrect
                        challenge_statuses[challenge_number] = 'Incorrect'

                        # Loop through the lines of the file
                        for line in lines:
                            # If the has a line with \tCORRECT
                            if '\tCORRECT' in line:
                                # Update the challenge status to Correct
                                challenge_statuses[challenge_number] = 'Correct'
                                break

    # Return the found statuses
    return challenge_statuses


def post_execution(files, suggestions_location, challenge_location, execution_file):
    def write_to_log(suggestions_location, new_file, results):
        with open(suggestions_location + '__status__.txt', 'a') as f:
            f.write('\n' + new_file + '\t' + results)
    
    def set_results_color(result):
        if result == 'CORRECT' or result == 'TEST CORRECT':
            return 'green'
        return 'red'

    # Fetch user requested input data
    user_test_input = request.form.get('user_test_input')
    number_of_random_tests = request.form.get('number_of_random_tests')

    try:
        # Fetch the POST-data
        posted = request.form['form_input'].replace('\r', '').replace('    ', '\t')

        # Add newline if missing from the posted data
        if not posted.endswith('\n'):
            posted = posted + '\n'
        
        # New suggestion file name setup
        new_file = 'suggestion_' + str(len(files) - 2) + '.py'

        # Write POST contents to new file
        with open(suggestions_location + new_file, 'w') as f:
            f.write(posted)

        try:
            # Specify challenge to execute
            challenge_number = challenge_location.split('/')[-1].replace('challenge_', '')

            # Run the new suggestion file and fetch output
            process_output = subprocess.check_output(
                ['python', str(execution_file),
                 '-c', str(challenge_number),
                 '-f', str(new_file),
                 '-t', str(user_test_input),
                 '-n', str(number_of_random_tests)
                ], stderr=subprocess.STDOUT, shell=True).decode('utf-8')
        except subprocess.CalledProcessError as e:
            # Fetch error message and print to console
            error_message = 'Runtime error:\n' + (e.output).decode('utf-8')

            # Update log
            write_to_log(suggestions_location, new_file, 'ERROR')

            # Return error message to page
            return posted, error_message, '', 'gray', user_test_input, number_of_random_tests

        # Format output
        output_debug = process_output.split('---RESULTS---')[0]
        output_answer = process_output.split('---RESULTS---')[1]

        # Update history
        results = output_answer.split('\n')[-2].replace('\r', '')

        # Update log
        write_to_log(suggestions_location, new_file, results)

        # Get the color of the textarea
        result_color = set_results_color(results)
        
    except:
        # Initialize output variable
        posted = ''
        output_debug = ''
        output_answer = ''
        result_color = ''

    # Return data
    return posted, output_debug, output_answer, result_color, user_test_input, number_of_random_tests


def fetch_contents(challenge_location, execution_file):
    # Fetch all files in folder as list
    def list_files(folder):
        for root, dirs, files in os.walk(folder):
            return files

    # Try to convert variable to integer
    def tryint(s):
        try:
            return int(s)
        except ValueError:
            return s

    # Return the integer value in the filenames
    def filename_integer(s):
        return [tryint(c) for c in re.split('([0-9]+)', s)]

    # Prepare page contents by loading necessary data
    suggestions_location = challenge_location + '/suggestions/'

    # Get the latest file (highest integer)
    files = list_files(suggestions_location)
    sorted_filenames = sorted(files, key=filename_integer)

    # Check if template should be loaded or the latest suggestion file
    if len(sorted_filenames) == 3:
        filename = sorted_filenames[-1]
    else:
        filename = sorted_filenames[-2]

    # Read the contents of the latest file
    with open(suggestions_location + filename, 'r') as f:
        contents = f.read()
    
    # POST execution
    posted, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = post_execution(
        files, suggestions_location, challenge_location, execution_file)

    # Update contents    
    if not posted == '':
        contents = posted
    
    # Return page contents
    return contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests
