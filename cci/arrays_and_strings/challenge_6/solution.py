def solution(input_string):
    # Initialize variables
    result_string = ''
    current_character = ''
    current_character_length = 0

    # Iterate through all characters in string input
    for i in range(0, len(input_string)):
        # Add 1 to current character length
        current_character_length += 1

        # Check if current character is different
        if current_character != input_string[i]:
            # First round
            if current_character == '':
                current_character = input_string[i]
                current_character_length = 0
            # Other rounds
            else:
                # Add to result string
                result_string += (current_character + str(current_character_length))

                # Reset values for next character change
                current_character = input_string[i]
                current_character_length = 0
    
    # Add to result string
    result_string += (current_character + str(current_character_length+1))

    # Check if length of result_string is shorter than input string
    if len(result_string) < len(input_string):
        return result_string

    return input_string
