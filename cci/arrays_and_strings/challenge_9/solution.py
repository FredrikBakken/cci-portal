def solution(input_string_1, input_string_2):
    # Check if the length of the two input strings are the same
    if len(input_string_1) != len(input_string_2):
        return False
    
    # Fetch clock rotation value (modular)
    clock_rotation = len(input_string_1)

    # Initial string value
    initial_character = input_string_1[0]

    # Define all potential starting indexes
    start_indexes = []

    # Find all potential starting points
    for i in range(0, len(input_string_2)):
        if input_string_2[i] == initial_character:
            start_indexes.append(i)

    # Check if any starting points exist
    if len(start_indexes) == 0:
        return False

    # Initialize string rotation found variable
    string_rotation_found = False
    
    # Loop through each potential start index
    for index in start_indexes:
        # Loop through each index in first input string
        for i in range(0, len(input_string_1)):
            # Specify current index
            current_index = (index + i) % clock_rotation

            # Break out of current loop if characters are not equal
            if input_string_1[i] != input_string_2[current_index]:
                break
            
            # If all characters are equal and end of index has been reached
            if ((input_string_1[i] == input_string_2[current_index]) and (i == len(input_string_1) - 1)):
                string_rotation_found = True
    
    # Return string rotation results
    return string_rotation_found
