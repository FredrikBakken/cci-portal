def remove_duplicates(head):
    # Define current node
    current = head
    
    # Loop until the end of the linked list
    while current != None:
        print(current.data)
        current = current.next
    
    # Return the head of the linked list
    return head
