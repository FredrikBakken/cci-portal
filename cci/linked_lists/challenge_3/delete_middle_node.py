'''
Crack the Coding Interview 6th Edition

Data Structures | Chapter 2 | Linked Lists

Challenge 3: Delete Middle Node
Implement an algorithm to delete a node in the middle (i.e., any
node but the first and last node, not necessarily the exact middle)
of a singly linked list, given only access to that node.
'''

from copy import deepcopy

from challenge_3.solution import solution


def execute_challenge_3(module, generate_known_test, generate_random_test, linked_list_to_string, print_results, custom_test_input, number_of_random):
    print('todo')
