# Imports
import argparse

from importlib import import_module

from support import Node, generate_known_test, generate_random_test, linked_list_to_string, print_results, custom_test_setup, number_of_random_runs

from challenge_1.remove_duplicates import execute_challenge_1
from challenge_2.return_kth_to_last import execute_challenge_2
from challenge_3.delete_middle_node import execute_challenge_3


if __name__ == '__main__':
    # Initialize argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-challenge', metavar='-c')
    parser.add_argument('-file', metavar='-f')
    parser.add_argument('-test', metavar='-t')
    parser.add_argument('-number', metavar='-n')
    args = parser.parse_args()

    # Fetch challenge as integer
    challenge = int(args.challenge)

    # Fetch file to load as module
    filename = str(args.file)

    # Fetch and format user's test input
    custom_test_input = str(args.test)
    custom_test_input = custom_test_setup(custom_test_input, challenge)

    # Fetch the number of random tests
    number_of_random = str(args.number)
    number_of_random = number_of_random_runs(number_of_random, custom_test_input)

    # Load the correct module
    module_name = 'challenge_' + str(challenge) + '.suggestions.' + filename.replace('.py', '')
    module = import_module(module_name)

    # Execute challenge 1
    if challenge == 1:
        execute_challenge_1(module, generate_known_test, generate_random_test, linked_list_to_string, print_results, custom_test_input, number_of_random)

    # Execute challenge 2
    elif challenge == 2:
        execute_challenge_2(module, generate_known_test, generate_random_test, linked_list_to_string, print_results, custom_test_input, number_of_random)
    
    # Execute challenge 3
    elif challenge == 3:
        execute_challenge_3(module, generate_known_test, generate_random_test, linked_list_to_string, print_results, custom_test_input, number_of_random)
    
    # Execute challenge 4
    elif challenge == 4:
        print('todo')
    
    # Execute challenge 5
    elif challenge == 5:
        print('todo')
    
    # Execute challenge 6
    elif challenge == 6:
        print('todo')
    
    # Execute challenge 7
    elif challenge == 7:
        print('todo')

    # Execute challenge 8
    elif challenge == 8:
        print('todo')
