# CCI Portal

![python-logo](https://www.python.org/static/img/python-logo.png)


CCI Portal is a web application built on the Python [Flask](http://flask.pocoo.org) framework. It is designed to make it easy to get started on the coding challenges found in the book [Cracking the Coding Interview](http://www.crackingthecodinginterview.com) by [Gayle Laakmann McDowell](http://www.gayle.com).

The project was inspired by the book's lack of surrounding Python code base, where everyone would have to setup their own test cases and suggestive solutions, without being able to easily confirm if the suggestion is correct or not.

**Be aware:** Project is still under development and contributions are welcome. If any bugs, errors, or issues are found, please create a [New Issue](https://gitlab.com/FredrikBakken/cci-portal/issues) or a [New Merge Request](https://gitlab.com/FredrikBakken/cci-portal/merge_requests).

## Installation

1. Install and configure Python (application was developed with ```Python 3.6```).
2. Install the requirements: ```python -m pip install -r requirements.txt```.

## Start the Web Application

Run the web application by executing the command ```python app.py``` in the terminal, then access the application by going to ```http://localhost:5000``` in the web browser.

## Showcase

Accessing Challenges          |  Solving Challenges
:----------------------------:|:------------------------------:
![](gifs/cci-portal_gif1.gif) | ![](gifs/cci-portal_gif2.gif)