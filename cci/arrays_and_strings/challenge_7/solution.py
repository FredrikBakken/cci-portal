def solution(matrix):
    # Number of iterations
    iterations = len(matrix) // 2

    for i in range(iterations):
        for j in range(i, len(matrix) - 1 - i):
            # Top => Right
            temp_1 = matrix[j][-1-i]
            matrix[j][-1-i] = matrix[i][j]

            # Right => Bottom
            temp_2 = matrix[-1-i][-1-j]
            matrix[-1-i][-1-j] = temp_1

            # Bottom => Left
            temp_1 = matrix[-1-j][i]
            matrix[-1-j][i] = temp_2

            # Left => Top
            temp_2 = matrix[i][j]
            matrix[i][j] = temp_1

    # Return rotated matrix
    return matrix
