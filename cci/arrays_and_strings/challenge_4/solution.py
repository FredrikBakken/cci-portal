def solution(input_string):
    # Even input string length
    if len(input_string) % 2 == 0:
        first_half = input_string[:len(input_string) // 2]
        second_half = input_string[len(input_string) // 2:]
    
    # Odd input string length
    else:
        middle = len(input_string) // 2
        first_half = input_string[:(middle)]
        second_half = input_string[(middle + 1):]

    # Sort first and second half
    sorted_first_half = sorted(first_half)
    sorted_second_half = sorted(second_half)

    # Check if first and second half is equal
    if sorted_first_half == sorted_second_half:
        # Return True (palindrome permutation)
        return True
    
    # Return False (not palindrome permutation)
    return False
