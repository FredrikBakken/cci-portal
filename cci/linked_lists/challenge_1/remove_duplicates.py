'''
Crack the Coding Interview 6th Edition

Data Structures | Chapter 2 | Linked Lists

Challenge 1: Remove Duplicates
Write code to remove duplicates from an unsorted linked list. How would
you solve this problem if a temporary buffer is not allowed?
'''

from copy import deepcopy

from challenge_1.solution import solution


def check_duplicates(head):
    # Initialize content list
    contents = []

    # Set current variable to head
    current = head

    # Loop through entire linked list
    while current != None:
        # Check if current value exist in content list
        if current.data in contents:
            return False
        
        # Append current.data to content list
        contents.append(current.data)
        
        # Update current node to next node
        current = current.next
    
    return True


def execute_challenge_1(module, generate_known_test, generate_random_test, linked_list_to_string, print_results, custom_test_input, number_of_random):
    if isinstance(custom_test_input, str):
        custom_test = False
        
        # Generate known tests
        tests = [
            generate_known_test(elements=[1, 2, 3, 4, 5, 6, 7, 8, 9]),
            generate_known_test(elements=['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']),
            generate_known_test(elements=['D', 'E', 'C', 'C', 'B', 'D', 'A', 'E']),
            generate_known_test(elements=[42, 65, 99, 0, 3, 82, 47, 90, 2, 8, 4, 65]),
            generate_known_test(elements=[1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7])
        ]
    else:
        custom_test = True

        # Generate custom test
        tests = custom_test_input
    
    # Generate random tests
    for _ in range(number_of_random):
        tests.append(generate_random_test(minLength=10, maxLength=20))

    # Deepcopy tests to create new memory allocations
    test_solution = deepcopy(tests)
    test_suggestion = deepcopy(tests)
    
    # Initialize results
    results = []

    # Loop through each test
    for index, test in enumerate(tests):
        # Fetch solution linked list
        answer = solution(test_solution[index])

        # Fetch suggestion linked list
        suggestion = module.remove_duplicates(test_suggestion[index])

        # Check for duplicates in linked list suggestion
        duplicates_exist = check_duplicates(suggestion)

        # Input / Output as string variable
        test_input = linked_list_to_string(test)
        test_answer = linked_list_to_string(answer)
        test_output = linked_list_to_string(suggestion)

        # Append current test results to results
        results.append([duplicates_exist, test_input, test_answer, test_output])

    # Print the results
    print_results(results, custom_test, number_of_random, ch1=True)
