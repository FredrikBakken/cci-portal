'''
Crack the Coding Interview 6th Edition

Data Structures | Chapter 2 | Linked Lists

Challenge 2: Return Kth to Last
Implement an algorithm to find the kth to last element of a singly
linked list.
'''

from copy import deepcopy

from challenge_2.solution import solution


def execute_challenge_2(module, generate_known_test, generate_random_test, linked_list_to_string, print_results, custom_test_input, number_of_random):
    if isinstance(custom_test_input, str):
        custom_test = False
        
        # Generate known tests
        tests = [
            [0, generate_known_test(elements=[1, 2, 3, 4, 5, 6, 7, 8, 9])],
            [4, generate_known_test(elements=['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'])],
            [-1, generate_known_test(elements=['D', 'E', 'C', 'C', 'B', 'D', 'A', 'E'])],
            [9, generate_known_test(elements=[42, 65, 99, 0, 3, 82, 47, 90, 2, 8, 4, 65])],
            [15, generate_known_test(elements=[1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7])]
        ]
    else:
        custom_test = True

        # Generate custom test
        tests = custom_test_input
    
    # Generate random tests
    for _ in range(number_of_random):
        tests.append(generate_random_test(minLength=10, maxLength=20, kth=True))
    
    # Deepcopy tests to create new memory allocations
    test_solution = deepcopy(tests)
    test_suggestion = deepcopy(tests)

    # Initialize results
    results = []

    # Loop through each test
    for index, test in enumerate(tests):
        # Fetch solution linked list
        answer = solution(test_solution[index][1], test_solution[index][0])

        # Fetch suggestion linked list
        suggestion = module.return_kth_to_last(test_suggestion[index][1], test_suggestion[index][0])

        # Input / Output as string variable
        test_input = [test[0], linked_list_to_string(test[1])]
        test_answer = answer
        test_output = suggestion

        # Append current test results to results
        results.append([test_input, test_answer, test_output])

    # Print the results
    print_results(results, custom_test, number_of_random, ch2=True)
