def solution(head, k):
    # If k if outside of the linked list scope
    if k < 0:
        # Return None
        return None

    # Define the current and kth nodes
    current_node = head
    kth_node = None

    # Initialize counter
    counter = 0

    # Loop through linked list until next is None
    while current_node.next != None:
        # If counter is equal to kth value
        if counter == k:
            # Point kth node to head
            kth_node = head

        # If kth node is define
        if kth_node:
            # Update kth node to next
            kth_node = kth_node.next
        
        # Iterate counter
        counter += 1

        # Update current node to next
        current_node = current_node.next
    
    # If kth node is not defined
    if kth_node == None:
        # Return None
        return None

    # Return the kth node data
    return kth_node.data
