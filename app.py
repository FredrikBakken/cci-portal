
from flask import Flask, render_template, request

from backend import challenge_status, fetch_contents


# Define challenge locations
cci_arrays_and_strings  = 'cci/arrays_and_strings/'
cci_linked_lists        = 'cci/linked_lists/'
cci_stacks_and_queues   = 'cci/stacks_and_queues/'
cci_trees_and_graphs    = 'cci/trees_and_graphs/'


### Flask application setup
app = Flask(__name__)


### HOME / INDEX
@app.route('/')
def index():
    # Render the template
    return render_template('home.html')


### ARRAYS AND STRINGS
@app.route('/arrays-and-strings')
def arrays_and_strings():
    # Fetch the challenge statuses
    challenge_statuses = challenge_status(cci_arrays_and_strings)

    # Render the template
    return render_template('arrays-and-strings.html', challenge_statuses=challenge_statuses)


@app.route('/arrays-and-strings/is-unique', methods=['GET', 'POST'])
def is_unique():
    # Define the challenge location and execution file
    challenge_location = cci_arrays_and_strings + 'challenge_1'
    execution_file = cci_arrays_and_strings + 'arrays_and_strings.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('arrays-and-strings/is-unique.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="Hello world")


@app.route('/arrays-and-strings/check-permutation', methods=['GET', 'POST'])
def check_permutation():
    # Define the challenge location and execution file
    challenge_location = cci_arrays_and_strings + 'challenge_2'
    execution_file = cci_arrays_and_strings + 'arrays_and_strings.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('arrays-and-strings/check-permutation.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="Custom input 1;Custom input 2")


@app.route('/arrays-and-strings/urlify', methods=['GET', 'POST'])
def urlify():
    # Define the challenge location and execution file
    challenge_location = cci_arrays_and_strings + 'challenge_3'
    execution_file = cci_arrays_and_strings + 'arrays_and_strings.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('arrays-and-strings/urlify.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="Hello world")


@app.route('/arrays-and-strings/palindrome-permutation', methods=['GET', 'POST'])
def palindrome_permutation():
    # Define the challenge location and execution file
    challenge_location = cci_arrays_and_strings + 'challenge_4'
    execution_file = cci_arrays_and_strings + 'arrays_and_strings.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('arrays-and-strings/palindrome-permutation.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="Hello world")


@app.route('/arrays-and-strings/one-away', methods=['GET', 'POST'])
def one_away():
    # Define the challenge location and execution file
    challenge_location = cci_arrays_and_strings + 'challenge_5'
    execution_file = cci_arrays_and_strings + 'arrays_and_strings.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('arrays-and-strings/one-away.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="Custom input 1;Custom input 2")


@app.route('/arrays-and-strings/string-compression', methods=['GET', 'POST'])
def string_compression():
    # Define the challenge location and execution file
    challenge_location = cci_arrays_and_strings + 'challenge_6'
    execution_file = cci_arrays_and_strings + 'arrays_and_strings.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('arrays-and-strings/string-compression.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="Hello world")


@app.route('/arrays-and-strings/rotate-matrix', methods=['GET', 'POST'])
def rotate_matrix():
    # Define the challenge location and execution file
    challenge_location = cci_arrays_and_strings + 'challenge_7'
    execution_file = cci_arrays_and_strings + 'arrays_and_strings.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('arrays-and-strings/rotate-matrix.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="[[1, 2, 3], [4, 5, 6], [7, 8, 9]]")


@app.route('/arrays-and-strings/zero-matrix', methods=['GET', 'POST'])
def zero_matrix():
    # Define the challenge location and execution file
    challenge_location = cci_arrays_and_strings + 'challenge_8'
    execution_file = cci_arrays_and_strings + 'arrays_and_strings.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('arrays-and-strings/zero-matrix.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="[[1, 2, 3], [4, 0, 6], [7, 8, 9]]")


@app.route('/arrays-and-strings/string-rotation', methods=['GET', 'POST'])
def string_rotation():
    # Define the challenge location and execution file
    challenge_location = cci_arrays_and_strings + 'challenge_9'
    execution_file = cci_arrays_and_strings + 'arrays_and_strings.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('arrays-and-strings/string-rotation.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="Custom input 1;Custom input 2")



### LINKED LISTS
@app.route('/linked-lists')
def linked_lists():
    # Fetch the challenge statuses
    challenge_statuses = challenge_status(cci_linked_lists)

    # Render the template
    return render_template('linked-lists.html', challenge_statuses=challenge_statuses)


@app.route('/linked-lists/remove-duplicates', methods=['GET', 'POST'])
def remove_duplicates():
    # Define the challenge location and execution file
    challenge_location = cci_linked_lists + 'challenge_1'
    execution_file = cci_linked_lists + 'linked_lists.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('linked-lists/remove-duplicates.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="1, A, 2, B, 3, C")


@app.route('/linked-lists/return-kth-to-last', methods=['GET', 'POST'])
def return_kth_to_last():
    # Define the challenge location and execution file
    challenge_location = cci_linked_lists + 'challenge_2'
    execution_file = cci_linked_lists + 'linked_lists.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('linked-lists/return-kth-to-last.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="3; 1, A, 2, B, 3, C")


@app.route('/linked-lists/delete-middle-node', methods=['GET', 'POST'])
def delete_middle_node():
    # Define the challenge location and execution file
    challenge_location = cci_linked_lists + 'challenge_3'
    execution_file = cci_linked_lists + 'linked_lists.py'

    # Fetch content to present on challenge page
    contents, output_debug, output_answer, result_color, user_test_input, number_of_random_tests = fetch_contents(
        challenge_location, execution_file)

    # Render the HTML page
    return render_template('linked-lists/delete-middle-node.html', contents=contents, output_debug=output_debug,
        output_answer=output_answer, result_color=result_color, user_test_input=user_test_input,
        number_of_random_tests=number_of_random_tests, placeholder="1, A, 2, B, 3, C")



### STACKS AND QUEUES
@app.route('/stacks-and-queues')
def stacks_and_queues():
    # Fetch the challenge statuses
    challenge_statuses = challenge_status(cci_stacks_and_queues)

    # Render the template
    return render_template('stacks-and-queues.html', challenge_statuses=challenge_statuses)



### TREES AND GRAPHS
@app.route('/trees-and-graphs')
def trees_and_graphs():
    # Fetch the challenge statuses
    challenge_statuses = challenge_status(cci_trees_and_graphs)

    # Render the template
    return render_template('trees-and-graphs.html', challenge_statuses=challenge_statuses)



if __name__ == "__main__":
    app.run()
